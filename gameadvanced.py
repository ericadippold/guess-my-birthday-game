from random import randint
from random import randrange

#implemented dict to pull from later when getting the random month generator
numToMonth = {
        1: 'January',
        2: 'February',
        3: 'March',
        4: 'April',
        5: 'May',
        6: 'June',
        7: 'July',
        8: 'August',
        9: 'September',
        10: 'October',
        11: 'November',
        12: 'December'
    }

name = input("Hi! What is your name?")

min_year = 1924
max_year = 2004
min_month = 1
max_month = 12
min_day = 1
max_day = 30
guess_counter = 1

#range could also be an input by the person... i.e. how many guesses do you want to implement?
for guesses in range(1,6):
    month = randint(min_month, max_month)
    year = randint(min_year, max_year)
    day = randint(min_day, max_day)
    guess = input("Guess " + str(guess_counter) + " : " + str(name) + " were you born on " + str(day) + " " +str(numToMonth[month]) + " " + str(year) + " ?" + " yes, later or earlier?")
    if guess == "yes":
        print("I knew it!")
        exit()
    elif guess_counter > 4:
        print("I have other things to do. Good bye.")
        exit()
    else:
        print("Drat Lemme try again!")
        guess_counter += 1
        if guess == "earlier":
            max_year = year
            #year selected is now going to equal the "max_year" in rand int generation
        else:
            min_year = year
            # year selected is now going to equal the "min_year" in rand int generation
