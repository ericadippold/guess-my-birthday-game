from random import randint

name = input("Hi! What is your name?")

min_year = 1924
max_year = 2004
min_month = 1
max_month = 12
guess_counter = 1

for guesses in range(1,6):
    month = randint(min_month, max_month)
    year = randint(min_year, max_year)
    guess = input("Guess " + str(guess_counter) + " : " + str(name) + " were you born in " + str(month) + " / " + str(year) + " ?" + " yes or no?")
    if guess == "yes":
        print("I knew it!")
        exit()
    elif guess == "no" and guess_counter > 4:
        print("I have other things to do. Good bye.")
        exit()
    else:
        print("Drat Lemme try again!")
        guess_counter += 1
